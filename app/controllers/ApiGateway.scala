package controllers

import javax.inject.{Inject, Singleton}

import akka.actor.ActorSystem
import play.api.libs.concurrent.CustomExecutionContext
import play.api.libs.ws.{WSClient}
import play.api.mvc._
import services.GatewayService

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext}

@Singleton
class ApiGateway @Inject()(cc: ControllerComponents, ws: WSClient, gatewayService: GatewayService) extends AbstractController(cc){

  trait MyExecutionContext extends ExecutionContext

  class MyExecutionContextImpl @Inject()(system: ActorSystem)
    extends CustomExecutionContext(system, "my.executor") with MyExecutionContext

  def get(url: String) = Action.async { request =>
      val serviceUrl = gatewayService.findServiceUrl(url)

      val heads = request.headers.toSimpleMap
      val res =  ws.url(serviceUrl).withHttpHeaders(heads.toList:_*).get().map { response =>
        response
      }

      gatewayService.toResult(res)
  }

  def post(url: String) = Action.async { request =>
    val serviceUrl = gatewayService.findServiceUrl(url)
    val heads = request.headers.toSimpleMap

    val res = ws.url(serviceUrl).withHttpHeaders(heads.toList:_*).post(request.body.asJson.get).map { response =>
      response
    }
    gatewayService.toResult(res)
  }

  def put(url: String) = Action async { request =>
    val serviceUrl = gatewayService.findServiceUrl(url)
    val heads = request.headers.toSimpleMap

    val res = ws.url(serviceUrl).withHttpHeaders(heads.toList:_*).put(request.body.asJson.get).map { response =>
      response
    }
    gatewayService.toResult(res)
  }

  def delete(url: String) = Action async { request =>
    val serviceUrl = gatewayService.findServiceUrl(url)
    val heads = request.headers.toSimpleMap

    val res = ws.url(serviceUrl).withHttpHeaders(heads.toList:_*).delete().map { response =>
      response
    }
    gatewayService.toResult(res)
  }


}
