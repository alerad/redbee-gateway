package services

import javax.inject.Inject

import akka.util.ByteString
import play.Environment
import play.api.Configuration
import play.api.http.HttpEntity
import play.api.libs.ws.WSRequest
import play.api.mvc.{ResponseHeader, Result}
import scala.concurrent.ExecutionContext.Implicits.global


import scala.concurrent.{Future, Promise}

class GatewayService @Inject()(environment: Environment, conf: Configuration){


  def findServiceUrl(url: String): String = {
    val split = url.split("/")
    serviceDomain().get(split(0)) match {
      case None => ""
      case Some(domain) => domain + s"/${url}"
    }
  }

  def serviceDomain(): Map[String, String] = {
    val baseKeyName = environment.isDev match {
      case true => "services.urls.dev"
      case false => "services.urls.prod"
    }

    Map(
      "users" -> conf.get[String](baseKeyName+".users"),
      "boards"-> conf.get[String](baseKeyName+".boards"),
      "interests" -> conf.get[String](baseKeyName+".interests")
    )
  }

  def toResult(future: Future[WSRequest#Response]) : Future[Result] = {
    future.map{
      res => {
        val promise: Promise[Result] = Promise[Result]()

        val result = Result(
          header = ResponseHeader(res.status),
          body = HttpEntity.Strict(ByteString(res.body), res.header("Content-Type"))
        )
        result
      }
    }

  }
}
